<!DOCTYPE html>
<html lang="en" views="index">
<head>
	<meta charset="UTF-8">
	<title>Gromov-templates</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
</head>
<body class="content">
	<header class="header">

		<div class="header__content">
			<div class="entrance">
				<a href="#" class="entrance__link">Вход</a>|
				<a href="#" class="entrance__link">Регистрация</a>
			</div>
			
			<div class="header__info">
				<div class="logo">
					<img class="logo__img" src="img/header/logo.png" alt="МногоСна">
					<div class="logo__position">
						<svg class="logo__svg">
							<use xlink:href="#position"></use>
						</svg>
						<div class="logo__city">Ханты-Мансийск</div>
					</div>
				</div>
			
				<div class="search">
					<form action="#">
						<input type="text" placeholder="Поиск" class="search__input">
						<button class="search__btn">
							<svg class="search__svg">
								<use xlink:href="#search"></use>
							</svg>
						</button>
					</form>
			
					<div class="numb">
						<svg class="numb__svg">
							<use xlink:href="#call"></use>
						</svg>
						<p class="numb__tel">+7 495 278 08 91</p>
						<p class="numb__tel">8 800 700 05 34</p>
					</div>
				</div>
			
				<div class="control">
					<svg class="control__like">
						<use xlink:href="#like"></use>
					</svg>
					<p class="control__link">Избранное</p>
					<p class="control__notice">1</p>
			
			
					<svg class="control__filters">
						<use xlink:href="#filters1"></use>
					</svg>
					<p class="control__link">Сравнение</p>
					<p class="control__notice">1</p>
			
					<div class="time">
						<svg class="time__icon">
							<use xlink:href="#clock"></use>
						</svg>
						<div class="time__text">Пн.–вс. с 9:00 до 20:00</div>
					</div>
				</div>
			
				<div class="buy">
					<div class="buy__btn">
						<svg class="buy__icon">
							<use xlink:href="#basket"></use>
						</svg>Корзина
						<div class="buy__notice">2</div>
					</div>
					
					<a href="" class="buy__order">Закажите звонок</a>
				</div>
			</div>

			<nav class="menu">
				<a href="#" class="menu__item">Акции</a>
				<a href="#" class="menu__item">Оптовикам</a>
				<a href="#" class="menu__item">Гарантия</a>
				<a href="#" class="menu__item">О нас</a>
				<a href="#" class="menu__item">Отзывы</a>
				<a href="#" class="menu__item">Обмен и возврат</a>
				<a href="#" class="menu__item">Нестандартные изделия</a>
				<a href="#" class="menu__item">Доставка и оплата</a>
				<a href="#" class="menu__item">Контакты</a>
			</nav>
		</div>
		<!-- <div class="header__logo"><img src="img/header/logo.png" alt=""></div> -->
	</header>
		
	<!-- <svg class="intro__eye">
		<use xlink:href="#filters1"></use>
	</svg> -->

	<?php echo file_get_contents('img/svg-sprite.svg') ?>
	<script src="js/script.js"></script>
	<link rel="stylesheet" href="css/style.css">
</body>
</html>